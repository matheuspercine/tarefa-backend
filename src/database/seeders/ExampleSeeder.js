const User = require ('../../models/User');
const faker = require ('faker-br');

const seedExample = async function() {

    const users = [];

    for(let i = 0; i < 10; i++) {
        users.push({
            name: faker.name.firstName(),
            email: faker.internet.email(),
            password: faker.internet.password(10),
            date_of_birth: faker.date(),
            phone_number: faker.phone.phoneNumer(),
            gender: faker.gender(),
            createdAt: new Date(),
            updatedAt: new Date()
        });

        try {
            await User.sync ({force: true});
            await User.bulkCreate(users);
        }
        catch (err) { console.log(err); }
    }
}

module.exports = seedExample;