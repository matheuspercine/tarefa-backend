const { Router } = require('express');
const UserController = require('../controllers/userController/UserController');
const ProductController = require('../controllers/productController/ProductController');
const router = Router();

//routes for User
router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.put('/users/:id',UserController.update);
router.delete('/users/:id',UserController.destroy);

//routes for Product
router.get('/product',ProductController.index);
router.get('/product/:id',ProductController.show);
router.post('/product',ProductController.create);
router.put('/product/:id',ProductController.update);
router.delete('/product/:id',ProductController.destroy);
router.put('/productAddRole/:id',ProductController.addRelationship);
router.delete('/productRemoveRole/:id',ProductController.removeRelationship);

module.exports = router;