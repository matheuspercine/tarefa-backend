const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize"); 

const User = sequelize.define ('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    }, 

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING
    },

    gender: {
        type: DataTypes.STRING
    }
});

User.associate = function(models) {
    User.hasMany(models.Product); 
    //um usuário vende de 0-n produtos
}

module.exports = User;